package models

type Knowleges []struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Path string `json:"path"`
	Img  string `json:"img"`
	Item []struct {
		Title    string `json:"title"`
		Rank     int    `json:"rank"`
		Duration int    `json:"duration"`
		Link     string `json:"link"`
		Extra    string `json:"extra"`
		Img      string `json:"img"`
	} `json:"item"`
}
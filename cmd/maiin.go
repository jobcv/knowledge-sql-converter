package main

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"os"

	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/remotejob/knowledge-sql-converter/internal/models"
)

func main() {

	db, err := sql.Open("sqlite3", "knowleges.db")

	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	sts := `
DROP TABLE IF EXISTS knowleges;
CREATE TABLE knowleges(id INTEGER PRIMARY KEY, name TEXT, path TEXT,img TEXT);
DROP TABLE IF EXISTS items;
CREATE TABLE items(knowlegeid INTEGER,title text,rank int,duration int,link TEXT,extra TEXT,img TEXT); 

`
	_, err = db.Exec(sts)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("table cars created")

	file, _ := os.ReadFile("data/knowlledge.json")

	data := models.Knowleges{}

	_ = json.Unmarshal([]byte(file), &data)

	stm, _ := db.Prepare("insert into knowleges(id,name,path,img) values(?,?,?,?)")

	stmit, err := db.Prepare("insert into items(knowlegeid,title,rank,duration,link,extra,img) values(?,?,?,?,?,?,?)")	
	if err != nil {

		log.Fatalln(err)
	}

	for _, dt := range data {
		
		_,err =stm.Exec(dt.ID,dt.Name,dt.Path,dt.Img)
		if err !=nil {
			log.Fatalln(err)
		}

		for _,it := range dt.Item {

			_,err =stmit.Exec(dt.ID,it.Title,it.Rank,it.Duration,it.Link,it.Extra,it.Img)
			if err !=nil {
				log.Fatalln(err)
			}

			
		}
	}
}
